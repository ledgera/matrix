module gitlab.com/ledgera/matrix

go 1.16

require (
	gitlab.com/ledgera/sam v0.0.0-20220403015307-3970e848870c
	gonum.org/v1/gonum v0.11.0
	gorgonia.org/tensor v0.9.22
)
